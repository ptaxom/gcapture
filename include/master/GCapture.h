#pragma once
#include <semaphore.h>
#include <shared/flags.h>

typedef struct _LibraryState
{
    int total_captures;
    int fps;
    int width;
    int height;
    int mem_key_sem;
    int mem_sem_descriptor;
    sem_t* shared_semaphores;
    int mem_im_descriptor;
    void *shared_images;
    int *barrier_value;
} LibraryState;

int create_captures(const char *rtsp_urls[], const int urls_length, const int fps, const int width, const int height, const char* slave_path, int shared_mem_key);

void release_captures();

char *get_pictures_lock_all();

extern LibraryState* state;