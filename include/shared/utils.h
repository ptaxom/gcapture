#pragma once
#include <semaphore.h>
#include <shared/utils.h>

void print_semaphore_value(sem_t *semaphore, const char *sem_name);

void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result);