#pragma once
#include <gst/gst.h>
#include <semaphore.h>

typedef unsigned long long ull;

typedef struct _ClientPayload
{
    char *shared_space;
    GstSample* sample;
    size_t image_size;
    sem_t *semaphore;
} ClientPayload;

GstElement* get_appsink(GstPipeline* pipeline);
ull get_time();

void* copy_image_async_trywait(void* data);

GstElement* init_pipeline(const char *url, int fps, int width, int height, ClientPayload *payload);