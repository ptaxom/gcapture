import cv2
from Capture import CGCapture
from tqdm import tqdm

urls = ["rtsp://10.30.30.179:8555/test", "rtsp://127.0.0.1:8554/test"]
capture = CGCapture(urls, 10, 416, 416)

for _ in tqdm(range(2000)):
    images = capture.read()
    for url, im in zip(urls, images):
        cv2.imshow(url, im)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
capture.release()
cv2.destroyAllWindows()