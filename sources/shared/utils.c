#include <shared/utils.h>
#include <sys/sem.h>
#include <assert.h>
#include <stdio.h>

void print_semaphore_value(sem_t *semaphore, const char *sem_name)
{
    int value;
    sem_getvalue(semaphore, &value);
    printf("Semaphore %s value: %d\n", sem_name, value);
}

void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0) {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    } else {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return;
}