#include <master/GCapture.h>
#include <shared/utils.h>

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stddef.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

LibraryState* state = NULL;
void initialize_zero_semaphore();
void print_zero_semaphore_value()
{
    print_semaphore_value((sem_t*)state->shared_semaphores, "IPC barrier");
}

#define CREATE_STR_VALUE(X) \
    char str_##X[10]; \
    sprintf(str_##X , "%d", state->X ) ; 

#define DEFAULT_SLAVE_PATH "/home/ptaxom/work/MC_INDOOR/rtsp/gstreamer/build/slave"

int create_captures(const char *rtsp_urls[], const int urls_length, const int fps, const int width, const int height, const char* slave_path, int shared_mem_key)
{
    int semaphore_iterator = 0;
    pid_t pg_id = -1;
    int memkey_semaphores,
        memkey_images;
    if (shared_mem_key < 0)
        memkey_semaphores = DEFAULT_MEMKEY_SEMAPHORES;
    else
        memkey_semaphores = shared_mem_key;
    memkey_images = memkey_semaphores * 2;

    if (state == NULL)
    {
        state = malloc(sizeof(LibraryState));
        if (state == NULL)
        {
            fprintf(stderr, "Cannot allocate memory\n");
            goto malloc_error;
        }
        memset(state, 0, sizeof(LibraryState));
        // init fields
        state->total_captures = urls_length;
        state->width = width;
        state->height = height;
        state->fps = fps;
        state->mem_key_sem = memkey_semaphores;
        
        size_t captures_size = width * height * 3 * urls_length * sizeof(u_int8_t),
               sem_size = (1 + urls_length) * sizeof(sem_t) + sizeof(int);
        // creating area of shared memory for semaphores
        state->mem_sem_descriptor = shmget(memkey_semaphores, sem_size, IPC_CREAT | 0666);
        if (state->mem_sem_descriptor < 0)
        {
            perror("Cannot create shared memory for semaphores");
            goto shmget_error_sem;
        }
        // attaching to shared memory of semaphores
        state->shared_semaphores = shmat(state->mem_sem_descriptor, NULL, 0);
        if (state->shared_semaphores < 0)
        {
            perror("Cannot attach to shared memory for semaphores");
            goto shmat_error_sem;
        }
        state->barrier_value = (int*)&state->shared_semaphores[urls_length + 1];
        // creating area of shared memory for images
        state->mem_im_descriptor = shmget(memkey_images, captures_size, IPC_CREAT | 0666);
        if (state->mem_im_descriptor < 0)
        {
            perror("Cannot create shared memory for images");
            goto shmget_error_im;
        }
        // attaching to shared memory of images
        state->shared_images = shmat(state->mem_im_descriptor, NULL, 0);
        if (state->shared_images < 0)
        {
            perror("Cannot attach to shared memory for images");
            goto shmat_error_im;
        }
        // creating POSIX semaphores
        *state->barrier_value = urls_length;
        for(; semaphore_iterator < 1 + urls_length; ++semaphore_iterator)
        {
            // first semaphore is barrier synchronizer for all processes to compensate GSTREAMER ASYNC_STATE_CHANGE
            int semaphore_initial_value = 1;
            sem_t *semaphore = &state->shared_semaphores[semaphore_iterator];
            
            int status = sem_init(semaphore, IPC_SEMAPHORE, semaphore_initial_value);
            if (status < 0)
                {
                    fprintf(stderr, "Semaphore %d\n", semaphore_iterator);
                    perror("Cannot init semaphore");
                    goto sem_init_error;
                }
        }
        // launching tasks
        CREATE_STR_VALUE(total_captures)
        CREATE_STR_VALUE(fps)
        CREATE_STR_VALUE(width)
        CREATE_STR_VALUE(height)
        CREATE_STR_VALUE(mem_key_sem)
       
        for(int i = 0; i < urls_length; ++i)
        {
            char str_child_id[10];
            sprintf(str_child_id, "%d", i);
            pid_t pid = fork();
            if (pid == 0)
            {
                int status = -1;
                if (slave_path)
                {
                    status = execl(slave_path, "", str_mem_key_sem,
                        str_total_captures, str_child_id, rtsp_urls[i], str_fps,
                        str_width, str_height, NULL);
                }
                else
                {
                    status = execl(DEFAULT_SLAVE_PATH, "", str_mem_key_sem,
                        str_total_captures, str_child_id, rtsp_urls[i], str_fps,
                        str_width, str_height, NULL);
                }
                    
                if (status < 0)
                {
                    perror("Cannot exec");
                    exit(1);
                }
                exit(0);
            }            
        }
        // waiting 40s for initialization
        printf("Waiting child processes\n");
        sleep(10);
        pg_id = getpgrp();
        for(int tries = 0; tries < urls_length * 2; tries++)
        {
            struct timespec delay;
            clock_gettime(CLOCK_REALTIME, &delay);
            delay.tv_sec += 40;

            if (sem_timedwait(state->shared_semaphores, &delay) < 0)
            {
                perror("Cannot wait for child initialization");
                goto child_wait_error;
            }
            else
            {
                printf("%d cameras not inited.\n", *state->barrier_value);
                if (*state->barrier_value == 0)
                    break;
                sem_post(state->shared_semaphores);
                sleep(5);
            }
        }
        if (*state->barrier_value == 0)
            printf("All childs inited\n");
        else
            {
                fprintf(stderr, "Cannot init all childs.\n");
                goto child_wait_error;
            }

        return 0;
    }
    else
    {
        return 1;       
    }

child_wait_error:
    if (pg_id > 0)
        killpg(pg_id, SIGKILL);
sem_init_error:
    semaphore_iterator -= 1;
    for(;semaphore_iterator >= 0; semaphore_iterator--)
    {
        sem_destroy((sem_t*)(state->shared_semaphores + sizeof(sem_t) * semaphore_iterator));
    }
    shmdt(state->shared_images);
shmat_error_im:
    shmctl(state->mem_im_descriptor, IPC_RMID, NULL);
shmget_error_im:
    shmdt(state->shared_semaphores);
shmat_error_sem:
    shmctl(state->mem_sem_descriptor, IPC_RMID, NULL);
shmget_error_sem:
    free(state);
malloc_error:
    return 1;
}


void release_captures()
{
    if (state)
    {
        signal(SIGUSR1, SIG_IGN);
        killpg(getpgrp(), SIGUSR1);
        for(int i = 0; i < state->total_captures + 1; i++)
        {
            sem_destroy((sem_t*)(state->shared_semaphores + sizeof(sem_t) * i));
        }
        shmdt(state->shared_semaphores);
        shmctl(state->mem_sem_descriptor, IPC_RMID, NULL);
        shmdt(state->shared_images);
        shmctl(state->mem_im_descriptor, IPC_RMID, NULL);
        free(state);
        printf("Released all.\n");
        state = NULL;
    }
}

char *get_pictures_lock_all()
{
    size_t pictures_size = state->total_captures * state->width * state->height * 3;
    char *pictures = malloc(pictures_size);
    sem_t* child_lockers = (sem_t*)(state->shared_semaphores + 1 * sizeof(sem_t));
    // for(int i = 0; i < state->total_captures; i++)
    //     sem_wait(&(child_lockers[i]));
    memcpy(pictures, state->shared_images, pictures_size);
    // for(int i = 0; i < state->total_captures; i++)
    //     sem_post(&(child_lockers[i]));
    return pictures;    
}