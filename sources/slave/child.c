#include <shared/utils.h>
#include <slave/gstutils.h>
#include <shared/flags.h>

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <semaphore.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <gst/gst.h>
#include <signal.h>

int total_captures;
int child_id;
int width;
int fps;
int height;
int memkey;


void *attach_to_memory(int mem_descriptor, size_t mem_size)
{
    mem_descriptor = shmget(mem_descriptor, mem_size, 0666);
    if (mem_descriptor < 0)
    {
        perror("Cannot get shared memory\n");
        exit(1);
    }
    void *shared_memory = shmat(mem_descriptor, NULL, 0);
    if (shared_memory < 0)
    {
        perror("Cannot attach to memory\n");
        exit(1);
    }
    return shared_memory;
}
long diffs = 0, n = 0;
static struct timespec start, end, diff;

void make_measure()
{
  clock_gettime(CLOCK_REALTIME, &end);
  timespec_diff(&start, &end, &diff);
  diffs += (diff.tv_sec % 10) * 1000 + (diff.tv_nsec / 1000000);
  n += 1;
  start = end;
}

static GstFlowReturn new_sample (GstElement *sink, ClientPayload *payload) {
  GstSample *sample; 
  
  g_signal_emit_by_name (sink, "pull-sample", &sample);
  if (sample) {
    int can_lock = sem_trywait(payload->semaphore);
    if (can_lock >= 0)
    {
    #ifdef TIME_MEASURE
        make_measure();
    #endif
      GstBuffer* buffer = gst_sample_get_buffer(sample);
      char* data;
      gsize size;
      gst_buffer_extract_dup(buffer, 0, payload->image_size, &data, &size);
      memcpy(payload->shared_space, data, payload->image_size);
      g_free(data);
      sem_post(payload->semaphore);
    }
    gst_sample_unref(sample);
    return GST_FLOW_OK;
  }
  return GST_FLOW_ERROR;
}

GstElement *pipeline;
ClientPayload *payload;

void sig_handler()
{
    printf("Catched signal. Stopping...");
    float avg_time = (float)diffs / n;
    printf("Average call time: %f\n", avg_time);

    gst_element_set_state (pipeline, GST_STATE_NULL);
    gst_object_unref (pipeline);
    free(payload);
}

int main(int argc, char *argv[])
{
    if (argc != 8)
    {
        fprintf(stderr, "Usage [sem_mem_key] [total_captures] [child_id] [url] [fps] [width] [height]");
        exit(1);
    }
    memkey = atoi(argv[1]);
    total_captures = atoi(argv[2]);
    child_id = atoi(argv[3]);
    fps = atoi(argv[5]);
    width = atoi(argv[6]);
    height = atoi(argv[7]);
    size_t captures_size = width * height * 3 * total_captures * sizeof(u_int8_t),
               sem_size = (1 + total_captures) * sizeof(sem_t);

    sem_t *semaphore_mem = attach_to_memory(memkey, sem_size);
    void *image_mem = attach_to_memory(memkey * 2, captures_size);
    
    payload = malloc(sizeof(ClientPayload));
    payload->image_size = width * height * 3;
    payload->semaphore = &semaphore_mem[child_id+1];
    payload->shared_space = image_mem + child_id * payload->image_size;
    payload->sample = NULL;
    printf("CHILD %d with url %s\n", child_id, argv[4]);
    int ret = 0;
    sigset_t set;
	ret = sigemptyset(&set);
	ret = sigaddset(&set, SIGUSR1);
    ret = signal(SIGUSR1, sig_handler);
    
    gst_init (&argc, &argv);
    pipeline = init_pipeline(argv[4], fps, width, height, payload);
    if (pipeline)
    {
        int status = sem_wait(semaphore_mem);
        int *barrier_value = (int*)&semaphore_mem[total_captures + 1];
        *barrier_value -= 1;
        status = sem_post(semaphore_mem);

        if (status < 0)
        {
            perror("Cannot post semaphore");
            exit(1);
        }
        GstElement *appsink = get_appsink(pipeline);
        #ifdef TIME_MEASURE
            clock_gettime(CLOCK_REALTIME, &start);
        #endif
        
        g_signal_connect (appsink, "new-sample", G_CALLBACK (new_sample), payload);

        int info;
        printf("Waiting for signal\n");
        sigwait(&set, &info);
        printf("Catched signal. Stopping...\n");
        #ifdef TIME_MEASURE
            float avg_time = (float)diffs / n;
            printf("Average call time: %fms\n ", avg_time);
        #endif

        gst_element_set_state (pipeline, GST_STATE_NULL);
        gst_object_unref (pipeline);
        printf("EXITED CHILD %d.\n", child_id);
    }
    free(payload);
    return 0;
}