#include <slave/gstutils.h>
#include <shared/vis.h>
#include <shared/utils.h>
#include <shared/flags.h>

#include <string.h>
#include <stdio.h>
#include <pthread.h>

GstElement* get_appsink(GstPipeline* pipeline)
{
  gint num_children = pipeline->bin.numchildren;
  GList *children = pipeline->bin.children;
  GstElement *element;
  gboolean finded_appsink = FALSE;
  for(int i = 0; i < pipeline->bin.numchildren && finded_appsink == FALSE; ++i)
  {
    element = (GstElement*)children->data;
    gchar *name = gst_object_get_name((GstObject*)element);
    // WTF, g_strv_contains segfault
    finded_appsink = (strstr(name, "appsink") != NULL);
    g_free(name);
    children = children->next;
  }
  if (!finded_appsink)
    element = NULL;
  return element;
}


ull get_time()
{
    struct timeval tv;

    gettimeofday(&tv, NULL);

    ull milliseconds_since_epoch =
      (unsigned long long)(tv.tv_sec) * 1000 +
      (unsigned long long)(tv.tv_usec) / 1000;
    return milliseconds_since_epoch;
}

GstElement* init_pipeline(const char *url, int fps, int width, int height, ClientPayload *payload)
{
    GstElement* pipeline;
    const char str_pipeline[1024];
    #ifdef H265
      sprintf(str_pipeline, "rtspsrc location=%s ! rtph265depay ! decodebin ! videoconvert ! video/x-raw,format=(string)BGR ! videoconvert ! videoscale ! video/x-raw,width=%d,height=%d ! videorate ! video/x-raw,framerate=%d/1 ! appsink emit-signals=true sync=false max-buffers=1 drop=true", url, width, height, fps);
    #else
      sprintf(str_pipeline, "rtspsrc location=%s ! decodebin ! videoconvert ! video/x-raw,format=(string)BGR ! videoconvert ! videoscale ! video/x-raw,width=%d,height=%d ! videorate ! video/x-raw,framerate=%d/1 ! appsink emit-signals=true sync=false max-buffers=1 drop=true", url, width, height, fps);
    #endif
    // pipeline = gst_parse_launch("rtspsrc location=rtsp://10.30.30.179:8555/test ! decodebin ! videoconvert ! video/x-raw,format=(string)BGR ! videoconvert ! videoscale ! video/x-raw,width=416,height=416 ! videorate ! video/x-raw,framerate=30/1 ! appsink emit-signals=true sync=false max-buffers=1 drop=true", NULL);
    pipeline = gst_parse_launch(str_pipeline, NULL);

    gst_element_set_state(pipeline, GST_STATE_PLAYING);
    ull waiting_begin = get_time();
    // wait 30 seconds for initialization
    GstState current_state, pending_state;
    GstClockTime wait_time = (GstClockTime)(300 * 1000 * 1000); // 300ms for waiting changes
    while (get_time() - waiting_begin < 30 * 1000)
    {
        gst_element_get_state(pipeline, &current_state, &pending_state, wait_time);
        if (current_state == GST_STATE_PLAYING)
        break;
        g_usleep(300 * 1000); // 300ms for delay
    }
    if (current_state != GST_STATE_PLAYING)
    {
        fprintf(stderr, "Cannot change pipeline state to PLAY[%s]\n", url);
        gst_element_set_state (pipeline, GST_STATE_NULL);
        gst_object_unref (pipeline);
        return NULL;
    }
      printf("Changed pipeline state to PLAY\n");
    return pipeline;
}
