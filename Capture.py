import numpy as np
from ctypes import *
from tqdm import tqdm
import time 


lib = CDLL('build/libpyGCapture.so', RTLD_GLOBAL)
slave_path = "build/slave"
MEMKEY = 6000 - 3

create_captures = lib.create_captures
create_captures.argtypes = [POINTER(c_char_p), c_int, c_int, c_int, c_int, c_char_p, c_int] #urls, n_urls, fps, width, height

release_captures  = lib.release_captures

get_pictures_lock_all = lib.get_pictures_lock_all
get_pictures_lock_all.restype = POINTER(c_int8)


free = lib.free

class CGCapture:
    
    def __init__(self, urls, fps, width, height, slave_fps=30):
        self.fps = fps
        self.width = width
        self.height = height
        self.urls = urls
        self.mem_size = width * height * 3
        self.delay = 1. / fps
        self.t0 = time.time()
        
        urls_arr = (c_char_p * len(urls))()
        for i in range(len(urls)):
            urls_arr[i] = urls[i].encode('utf-8')
        status = create_captures(urls_arr, len(urls), slave_fps, width, height, slave_path.encode('utf-8'), MEMKEY)
        
        
    def grab_frames(self):
        images = []
        row_data = get_pictures_lock_all()
        for im_num in range(len(self.urls)):
            im = np.zeros((self.height, self.width, 3), dtype=np.uint8)
            arr = np.ascontiguousarray(im.flat, dtype=np.uint8)
            data = arr.ctypes.data_as(POINTER(c_byte))
            new_address = cast(addressof(row_data.contents) + self.mem_size * im_num, POINTER(c_char))
            memmove(data, new_address, self.mem_size)
            images.append(im)
        free(row_data)
        return images
            
    def release(self):
        release_captures()

    def read(self):
        while True:
            t1 = time.time()
            if t1 - self.t0 > self.delay:
                images = self.grab_frames()
                self.t0 = time.time()
                break
        return images